#!/usr/bin/env python
'''
 ********************************************************************************
 **                                                                            **
 **  CEWR (Compiler Error and Warning Reporter):                               **
 **        A very simple python script that generates                          **
 **        compiler error - warning report in html format.                     **
 **        The generated report uses Bootstrap CSS framework for better        **
 **        interface. (see: http://getbootstrap.com)                           **
 **  Copyright (C) 2014 Gkratsia Tantilian                                     **
 **                                                                            **
 **  CEWR is free software: you can redistribute it and/or modify              **
 **  it under the terms of the GNU Lesser General Public License as            **
 **  published by the Free Software Foundation, either version 3 of the        **
 **  License, or (at your option) any later version.                           **
 **                                                                            **
 **  CEWR is distributed in the hope that it will be useful,                   **
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of            **
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             **
 **  GNU Lesser General Public License for more details.                       **
 **                                                                            **
 **  You should have received a copy of the GNU Lesser General Public License  **
 **  along with CEWR.  If not, see <http://www.gnu.org/licenses/>.             **
 **                                                                            **
 **  ------------------------------------------------------------------------  **
 **                                                                            **
 **              Author: Gkratsia Tantilian <sciencegt@gmail.com>              **
 **          University: Aristotle University of Thessaloniki                  **
 **                Date: March 2015                                            **
 **             Version: 1.1.0                                                  **
 **                                                                            **
 **  ------------------------------------------------------------------------  **
'''

import os
import sys
import getopt
import time
import subprocess
import string

def p_help():
        print "\n\
        *************************     CEWR v1.1.0     *************************\n\n\
        NAME        cewr - compiler error and warning reporter\n\n\
        USAGE       cewr [-o file.html][-m path/to/source][-p \"Project Name\"]\n\n\
        SYNOPSIS    Runs 'make' silently and keeps the error / warning output in a file. Parsing that file\n\
                    it creates an html report, including the user responsible for the respective line,\n\
                    if there is a repository in the source folder.\n\n\
        OPTIONS     -h: Displays this 'help' message\n\
                    -o: The path of html to be output. Defaults to the current folder with the name date.html\n\
                    -m: The path of the source code to be compiled\n\
                    -p: The project name to be displayed in the top right corner of the report. (optional)\n\
        EXAMPLE\n\
                    ./cewr -o index.html -m ../source/code/ -p\n\n\
        NOTES\n\
                    * The source folder should contain MakeFile\n\
                    * Uses GIT or Mercurial (hg) VCS. If both exist, GIT has the priority.\n\
        "

def BlameLine(make_path, filename, line):
    d = make_path
    f = filename
    line = str(line)

    auth_name = " - "
    auth_mail = " - "

    if os.path.exists(d+"/.git"):
        proc = subprocess.Popen(["cd "+d+"; git blame -L "+line+","+line+" -p "+f], stdout=subprocess.PIPE, shell=True)
        (cmd_out, cmd_err) = proc.communicate()
        cmd_out = string.split(cmd_out, "\n")
        auth_name = string.split(cmd_out[1], " ", 1)[1]
        auth_mail = string.split(cmd_out[2], " ")[1].replace("<", "").replace(">", "")
        auth_mail = "<a href='mailto:"+auth_mail+"'><i title='"+auth_mail+"' class='glyphicon glyphicon-envelope'></i></a>"
    elif os.path.exists(d+"/.hg"):
        proc = subprocess.Popen(["cd "+d+"; hg annotate -uw "+f+" | sed -n "+line+","+line+"p"], stdout=subprocess.PIPE, shell=True)
        (cmd_out, cmd_err) = proc.communicate()
        cmd_out = string.split(cmd_out, "<")
        cmd_out[0] = string.split(cmd_out[0], ":")[0]
        auth_name = cmd_out[0]
        if len(cmd_out) >= 2 and len(cmd_out[1]) > 2:
            auth_mail = cmd_out[1].replace(">", "")
            auth_mail = "<a href='mailto:"+auth_mail+"'><i title='"+auth_mail+"' class='glyphicon glyphicon-envelope'></i></a>"
        else:
            auth_mail = ""

    ret_val = "<strong>"+auth_name+"</strong> "+auth_mail
    return ret_val

def main():
    try:
        opts, args = getopt.getopt(sys.argv[1:], "o:m:p:h", ["output:", "make_path:", "project:", "help"])

    except getopt.GetoptError, err:
        print str(err)
        print str("Unable to proceed. Invalid options. Check the manual using: cewr.py -h\n")
        sys.exit(None)

    command = ""
    make_path = ""
    project_name = ""
    img_favicon = "./favicon.ico"
    img_logo = "./main_logo.png"
    out = "./"+time.strftime("%H_%M_%d_%m_%y")+".html"
    def_out = 0
    current_vcs = "<span style='color:#D63333'>VCS not found</span>"

    for option, argument in opts:
        if option in ("-h", "--help"):
            p_help()
            sys.exit(None)
        elif option in ("-o", "--output"):
            out = argument
            trgt_path = os.path.dirname(out) + "/"
            if trgt_path != "./":
                if not os.path.exists(trgt_path):
                    os.makedirs(trgt_path)
                cpy_favicon = "cp " + img_favicon + " " + trgt_path
                cpy_logo = "cp " + img_logo + " " + trgt_path
                os.system(cpy_favicon)
                os.system(cpy_logo)
        elif option in ("-m", "--make_path"):
            make_path = argument
        elif option in ("-p", "--project"):
            project_name = " | Project: <span style='color:#D63333'>"+argument+"</span>"

    if os.path.exists(make_path+"/.git"):
        current_vcs = "<span style='color:#D63333'>GIT</span>"
    elif os.path.exists(make_path+"/.hg"):
        current_vcs = "<span style='color:#D63333'>Mercurial</span>"

    if make_path != "":
        make_cmd = "make clean -C "+make_path+";make -C "+make_path+" 2> stderr.log"
    else:
        make_cmd = "make clean; make 2> stderr.log"

    os.system(make_cmd)

    stderr_f = open("stderr.log")
    cont_err = stderr_f.readlines()

    table_data = ""
    i = 0
    for line in cont_err:
        if 'warning:' in line.lower():
            i = i + 1
            tmp = line.split(":", 4)
            if len(tmp) == 5:
                user = BlameLine(make_path, tmp[0], int(tmp[1]))
                table_data += "<tr class='warning'><td>"+str(i)+"</td><td nowrap>"+user+"</td><td>"+tmp[1]+"</td><td>"+tmp[0]+"</td><td><strong>"+tmp[4].replace("<","&lt;").replace(">","&gt;")+"</strong></td></tr>"
        elif 'error:' in line.lower():
            i = i + 1
            tmp = line.split(":", 4)
            if len(tmp) == 5:
                user = BlameLine(make_path, tmp[0], int(tmp[1]))
                table_data += "<tr class='danger'><td>"+str(i)+"</td><td nowrap>"+user+"</td><td>"+tmp[1]+"</td><td>"+tmp[0]+"</td><td><strong>"+tmp[4].replace("<","&lt;").replace(">","&gt;")+"</strong></td></tr>"

    stderr_f.close()
    os.remove("stderr.log")

    report = "      <table class='table table-hover'>\n\
            <thead><tr><th>#</th><th>developer</th><th>line</th><th>file</th><th>message</th><tr></thead>\n\
            <tbody>"+table_data+"\n\
            </tbody>\n\
        </table>"

    content = "<!DOCTYPE html>\n\
        <html>\n\
        <head>\n\
            <meta charset='UTF-8'>\n\
            <title>Error and Warning Report "+time.strftime("%H:%M %d/%m/%y")+"</title>\n\
            <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css'>\n\
            <link rel='icon' href='"+img_favicon+"'>\n\
            <style type='text/css'>\n\
                body {\n\
                    background-color: #97AEB2;\n\
                    padding-top: 30px;\n\
                }\n\
            </style>\n\
        </head>\n\
        <body>\n\
            <div class='col-sm-8 col-sm-offset-1'>\n\
                <div class='text-center'><img src='"+img_logo+"' width='260px'/></div>\n\
                <div class='panel panel-default'>\n\
                    <div class='panel-heading'>\n\
                        <h3 class='panel-title'>\n\
                            <span class='text-danger'>Error</span> and <span class='text-warning'>Warning</span> Report ["+time.strftime('%H:%M %d/%m/%Y')+"]\n\
                            <div class='label label-info pull-right'>Version Control System: "+current_vcs+project_name+"</div>\n\
                        </h3>\n\
                    </div>\n\
                    <div class='panel-body'>"+report+"\n\
                    </div>\n\
                    <div class='panel-footer text-right'><span class='label label-info'>Gkratsia Tantilian &copy; "+time.strftime('%Y')+"</span></div>\n\
                </div>\n\
            </div>\n\
            <script src='https://code.jquery.com/jquery-2.1.1.min.js'></script>\n\
            <script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.conjugate1/js/bootstrap.min.js'></script>\n\
        </body>\n\
        </html>\n"

    out_f = open(out, 'w')
    out_f.write(content)
    out_f.close()

if __name__ == "__main__":
    main()
