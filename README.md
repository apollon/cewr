![logo.png](https://bitbucket.org/repo/Mzpz6r/images/1737200102-logo.png)

# CEWR v1.1.0
## Compiler Error and Warning Reporter

A very simple python script that generates compiler error - warning report in html format.
The generated report uses Bootstrap CSS framework for better interface. Supports both *GIT* and *Mercurial*.

(see: http://git-scm.com/ , http://mercurial.selenic.com/ , http://getbootstrap.com)

*USAGE*

```
./cewr [-o file.html][-m path/to/source][-p “Project Name”]
```

*EXAMPLE*

```
./cewr.py -o test.html -m ../mySourceCode/ -p “My Project”
```

*HELP*

For help type

```
./cewr.py -h
```

## Why is this useful?

Imagine the case that your team is working on a C/C++ project. After a while, as long as the project gets bigger and
bigger, some compiler warnings are starting to appear. You assume that the team members that are responsible for the
warnings, are about to fix them. But usually they don't.

So you can set up a cron job for example, which compiles your project using CEWR, every night. Now, every morning your
team is aware of compiler warnings (or even errors) as well as who is responsible for each of them.

Of course you can use blame / annotate and find out who must fix the errors / warnings. But this is a nice report
generator.
